<?php
	require_once dirname(__DIR__, 2) . '/TestFramework.php';

	class NaiveTest extends TestFramework
	{
		public function testRetrieval()
		{
			$pass = \Opcenter\Auth\Password::generate(16);
			$this->assertSame(16, \strlen($pass), 'Random string generated');
			$this->assertSame($pass, (new \Opcenter\Crypto\NaiveCrypt($pass))->get(), 'Password decrypted');
			return true;
		}

		public function testSerialization() {
			$pass = \Opcenter\Auth\Password::generate(16);
			$this->assertSame(16, \strlen($pass), 'Random string generated');
			$crypted = new \Opcenter\Crypto\NaiveCrypt($pass);
			$serialized = serialize($crypted);
			$this->assertSame(unserialize($serialized)->get(), $crypted->get(), 'Deserialization');
		}

	}