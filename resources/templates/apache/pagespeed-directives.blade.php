@if (HTTPD_PAGESPEED_PERSITE)
<IfModule pagespeed_module>
	{{-- chmod 5750 --}}
ModPagespeedFileCachePath "{{ $svc->getAuthContext()->domain_shadow_path() }}{{ \Opcenter\Provisioning\Apache::PAGESPEED_ROOT }}"
</IfModule>
@endif