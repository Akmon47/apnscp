<div class="ui-app-container col-md-9" id="ui-app-container">
	@includeWhen($Page->help_exists() && Page_Renderer::do_overview(), 'theme::partials.app.help')

	@includeWhen(($Page->is_postback || $Page->isDegraded()) && Page_Renderer::do_notifier(), 'theme::partials.app.postback.embed')
	<!--  app  -->
	<div class="ui-app row" id="ui-app">
		<div class="container-fluid">
			@php
				$app = $Page->display_application();
			@endphp
			@isset($app)
				@if (is_object($app))
					@hasSection('content')
						@yield('content')
					@else
						{{-- Laravel Blade --}}
						{!! $app->render() !!}
					@endif
				@else
					{{-- old format --}}
					@php include($app); @endphp
				@endif
			@endisset
		</div>
	</div>
</div>