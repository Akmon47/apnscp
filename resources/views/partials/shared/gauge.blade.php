<div class="ui-gauge-cluster d-flex {{ $gaugeClass ?? '' }}"
     @if (isset($gaugeId)) id="{{ $gaugeId }}" @endif>
	<div class="d-flex flex-column">
		<div class="ui-detailed-overlay"></div>
		<div id="ui-storage-label" class="ui-gauge-heading ui-gauge-label text-center">
			{{ $gaugeTitle ?? '' }}
		</div>
		<div class="ui-gauge">
			<div class="ui-gauge-used ui-gauge-slice" style="width: {{ sprintf("%.4f%%", min($fill/$max * 100, 100)) }}"></div>
			<div class="ui-gauge-free ui-gauge-slice"></div>
		</div>
	</div>
	<div class="ui-gauge-label ui-label-cluster flex-column align-self-center">
		<div class="">&nbsp;</div>
		<div class="ui-label-percentage">
			{{ sprintf('%u%%', min($fill/$max * 100, 100)) }}
		</div>
		<div class="ui-label-free">
			{{  sprintf('%u %s', max($max - $fill, 0), $unit ?? 'MB') }}
		</div>
	</div>
</div>