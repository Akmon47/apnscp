<?php

	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Metrics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	if (Schema::connection('pgsql')->hasTable('metrics_view')) {
    		return;
		}
		Schema::connection('pgsql')->create('metric_attributes', function (Blueprint $table) {
			$table->increments('attr_id', false);
			$table->string('name', 32);
			$table->string('label', 255);
			$table->enum('type', ['monotonic', 'value'])->default('value')->comment("Counter type");
		});

		Schema::connection('pgsql')->create('metrics', function (Blueprint $table) {
			$table->integer('attr_id', false);
			$table->integer('site_id', false, true)->nullable();
			$table->timestampTz('ts', 0);
			$table->integer('value')->comment('Metric value');
			$table->foreign('site_id')->references('site_id')->on('siteinfo')
				->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('attr_id')->references('attr_id')->on('metric_attributes')
				->onUpdate('cascade')->onDelete('cascade');
		});

		DB::connection('pgsql')->getPdo()->exec("CREATE INDEX attr_id_idx ON metrics USING HASH (attr_id);");
		DB::connection('pgsql')->getPdo()->exec("CREATE INDEX metrics_site_id_idx ON metrics USING HASH (site_id);");
		DB::connection('pgsql')->getPdo()->exec("SELECT create_hypertable('metrics', 'ts', chunk_time_interval  => interval '1 day');");

		DB::connection('pgsql')->getPdo()->exec("SELECT add_dimension('metrics', 'site_id', number_partitions => 4);");
		DB::connection('pgsql')->getPdo()->exec("CREATE INDEX site_id_idx ON metrics USING BRIN(site_id,ts) WITH (timescaledb.transaction_per_chunk);");
		DB::connection('pgsql')->getPdo()->exec("CREATE MATERIALIZED VIEW metrics_monotonic_view AS 
			SELECT
			  site_id,
			  ts,
			  (
				CASE
				  WHEN value >= lag(value) OVER w
					THEN value - lag(value) OVER w
				  WHEN lag(value) OVER w IS NULL THEN NULL
				  ELSE value
				END
			  ) AS \"value\",
			  \"name\"
			  FROM metrics
			  JOIN metric_attributes USING (attr_id)
			  WHERE 
			  type = 'monotonic' AND
			  ts >= NOW() - interval '1 day'
			  WINDOW w AS (ORDER BY ts ASC)");
		DB::connection('pgsql')->getPdo()->exec("CREATE MATERIALIZED VIEW metrics_value_view  AS 
			SELECT
			  site_id,
			  ts,
			  \"value\",
			  \"name\"
			  FROM metrics
			  JOIN metric_attributes USING (attr_id)
			  WHERE 
			  type = 'value' AND
			  ts >= NOW() - interval '1 day'
			  WINDOW w AS (ORDER BY ts ASC)");
		DB::connection('pgsql')->getPdo()->exec("CREATE INDEX metrics_monotonic_view_site_id_idx ON metrics_monotonic_view USING BRIN(site_id,ts);");
		DB::connection('pgsql')->getPdo()->exec("CREATE INDEX metrics_value_view_site_id_idx ON metrics_value_view USING BRIN(site_id,ts);");

		DB::connection('pgsql')->getPdo()->exec("CREATE VIEW metrics_view AS 
			  SELECT
				site_id,
				ts,
				\"value\",
				\"name\"
				FROM metrics_value_view
			  UNION
			  SELECT
				site_id,
				ts,
				\"value\",
				\"name\"
			  FROM metrics_monotonic_view;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::connection('pgsql')->drop('metrics_view');
        Schema::connection('pgsql')->drop('metrics_monotonic_view');
		Schema::connection('pgsql')->drop('metrics_value_view');
		Schema::connection('pgsql')->drop('metrics');
		Schema::connection('pgsql')->drop('metric_attributes');
    }
}
