<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Event;

	class Events
	{
		const FAILURE = 'fail';
		const SUCCESS = 'success';
		const CREATED = 'created';
		const DESTROYED = 'destroyed';
		const CHANGED = 'changed';
		const END = 'end';
	}