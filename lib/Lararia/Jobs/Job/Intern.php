<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs\Job;

	use Lararia\Jobs\Job;

	class Intern
	{
		/**
		 * @var Job completed/failed job
		 */
		protected $job;

		protected $log;

		protected $status;

		/**
		 * JobInterned constructor.
		 *
		 * @param Job        $job
		 * @param array|null $log
		 */
		public function __construct(Job $job, array $log = null)
		{
			$this->job = $job;

			if (null !== $log) {
				$job->setLog($log);
				$this->log = $log;
			} else {
				$this->log = $job->getLog();
			}
			assert(null !== $this->log, 'Error log is set');

		}

		public function getLog()
		{
			return $this->job->getLog();
		}

		public function success(): bool
		{
			return !$this->job->hasErrors();
		}

		public function getStatus(): int
		{
			return $this->job->getStatus();
		}

		public function getJob()
		{
			return $this->job;
		}
	}