<?php declare(strict_types=1);

/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2019
 */

namespace Module\Support\Webapps\VersionFetcher;

use Module\Support\Webapps\VersionFetcher;

class Github extends VersionFetcher {
	const VERSION_CHECK_BASE = 'https://api.github.com/repos';
	protected $mode = 'releases';
	public $versionField = 'name';

	public function setMode(string $mode): self {
		if ($mode !== 'tags' && $mode !== 'releases') {
			fatal("Unknown fetch mode `%s'", $mode);
		}
		$this->mode = $mode;
		return $this;
	}

	public function setVersionField(string $field): self {
		$this->versionField = $field;

		return $this;
	}

	/**
	 * Fetch versions from Github
	 *
	 * @param string $identifier
	 * @return array|null
	 */
	public function fetch(string $identifier): ?array
	{
		if (false === strpos($identifier, '/')) {
			error('Malformed identifier');
			return null;
		}
		$url = self::VERSION_CHECK_BASE . '/' . $identifier . '/' . $this->mode . '?per_page=1000';
		$opts = [
			'http' => [
				'method' => 'GET',
				'header' => [
					'User-Agent: ' . PANEL_BRAND . ' ' . APNSCP_VERSION,
				]
			]
		];
		$context = stream_context_create($opts);
		$contents = file_get_contents($url, false, $context);
		if (!$contents) {
			return array();
		}
		$versions = json_decode($contents, true);
		array_walk($versions, function (&$a) {
			$a['version'] = $a[$this->versionField];
			if ($a['version'][0] === 'v') {
				$a['version'] = substr($a['version'], 1);
			}
			if (!preg_match('/^\d+\.\d+\.\d+/', $a['version'])) {
				$a = [];
			}
		});
		$versions = array_filter($versions);
		usort($versions, static function ($a, $b) {
			if (version_compare($a['version'], $b['version'], '<')) {
				return -1;
			}
			if (version_compare($a['version'], $b['version'], '>')) {
				return 1;
			}

			return 0;
		});
		return $versions;
	}

}