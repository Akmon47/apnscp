<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2020
 */

namespace Module\Support\Webapps\App;

use Module\Support\Webapps\App\Type\Unknown\Handler as Unknown;
use Module\Support\Webapps\Contracts\ReconfigurableProperty;

class Reconfigurator
{
	use \NamespaceUtilitiesTrait;
	use \ContextableTrait;
	use \apnscpFunctionInterceptorTrait;

	const ALIAS_MAPS = [
		'clone' => 'duplicate'
	];

	/**
	 * @var Handler
	 */
	protected $app;
	protected ?\Deferred $callbackContext;

	protected function __construct(Unknown $app, ?\Deferred $context = null)
	{
		$this->app = $app;
		if (!$app->contextSynchronized($this->getAuthContext())) {
			fatal('Context mismatch');
		}
		$this->callbackContext = $context;
	}

	public function __destruct()
	{
		$this->app->getPane()->freshen(false);
	}

	/**
	 * Reconfigure property
	 *
	 * @param string $property
	 * @param mixed  $val
	 * @return bool
	 */
	public function reconfigure(string $property, &$val): bool
	{
		if (isset(self::ALIAS_MAPS[$property])) {
			$property = self::ALIAS_MAPS[$property];
		}

		if ($val === '0' || $val === '1') {
			$val = (bool)$val;
		} else if ($val === 'true') {
			$val = true;
		} else if ($val === 'false') {
			$val = false;
		}

		$instance = $this->handler($property);
		if ($instance->getValue() == $val && !$this->app->isInstalling()) {
			// no-op
			return true;
		}
		return $instance->handle($val) && ($this->isTransient($property) || $this->app->setOption($property, $val)) ?:
			error("Failed to reconfigure `%s' on %s", $property, $this->app->getPane()->getUrl());
	}

	/**
	 * Property should not be recorded
	 *
	 * @param string $property
	 * @return bool
	 */
	protected function isTransient(string $property): bool
	{
		return in_array($property, $this->app::TRANSIENT_RECONFIGURABLES, true);
	}

	/**
	 * Get original option value
	 *
	 * @return mixed
	 */
	public function getValue() {
		if ($this->isTransient($this->propertyFromClass())) {
			debug("All transient properties must implement getValue() for access");
			return null;
		}
		$property = strtolower(static::getBaseClassName());
		return $this->app->getOption($property);
	}

	/**
	 * Reconfiguration handler
	 *
	 * @param string $property
	 * @return $this
	 */
	public function handler(string $property): self
	{
		if (isset(self::ALIAS_MAPS[$property])) {
			$property = self::ALIAS_MAPS[$property];
		}

		if (null === ($class = $this->getReconfigurationClass($property))) {
			fatal("Unknown reconfiguration property `%(property)s' for %(app)s",
				['property' => $property, 'app' => $this->app->getName()]);
		}
		/** @var ReconfigurableProperty $class */
		$instance = $class::instantiateContexted($this->getAuthContext(), [$this->app]);
		if (!$instance instanceof ReconfigurableProperty) {
			fatal("Property `%(property)s' lacks ReconfigurableProperty", ['property' => $property]);
		}

		return $instance;
	}

	/**
	 * Register deferred execution as callee's discretion
	 *
	 * @param \Closure $fn
	 * @return $this
	 */
	protected function callback(\Closure $fn): self
	{
		defer($this->callbackContext, $fn);
		return $this;
	}

	/**
	 * Get runtime property from class
	 *
	 * @return string
	 */
	protected function propertyFromClass(): string
	{
		return strtolower(static::getBaseClassName());
	}

	/**
	 * Get property reconfiguration handler
	 *
	 * @param string $property
	 * @return string|null
	 */
	protected function getReconfigurationClass(string $property): ?string
	{
		$nses = [
			$this->app::appendNamespace('Reconfiguration'),
			Unknown::appendNamespace('Reconfiguration')
		];
		$suffix = '\\' . studly_case($property);
		foreach ($nses as $ns) {
			if (class_exists($ns . $suffix)) {
				return $ns . $suffix;
			}
		}

		return null;
	}
}
