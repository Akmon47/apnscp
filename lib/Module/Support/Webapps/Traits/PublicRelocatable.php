<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace Module\Support\Webapps\Traits;

	/**
	 * Trait PublicRelocatable
	 *
	 * Helper for apps that locate logic below docroot
	 *
	 * @package Module\Support\Webapps\Traits
	 */
	trait PublicRelocatable
	{
		protected function getAppRoot(string $hostname, string $path = ''): ?string
		{
			$docroot = $this->web_get_docroot($hostname, $path);
			// Ghost/Laravel app root resides 1 level down
			$normalized = $this->web_normalize_path($hostname, $path);
			if ($normalized && $docroot !== $normalized) {
				// symlinked...
				return \dirname($normalized);
			}

			return $docroot ? \dirname($docroot) : null;
		}
	}
