<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace CLI\Yum\Synchronizer\Plugins;

	/**
	 * Limit plugin scope to package-level
	 *
	 * When used, a plugin will only apply per-package instead to all packages
	 *
	 * @package CLI\Yum\Synchronizer\Plugins
	 *
	 */

	trait LimitPackage
	{
		/**
		 * Get plugin scope
		 *
		 * @return string
		 */
		public static function getScope(): string
		{
			return 'package';
		}
	}


