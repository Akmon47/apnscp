<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\AlternativesTrigger;

	/**
	 * Class Nss
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * @todo    support other packages
	 *
	 */
	class Jemalloc extends AlternativesTrigger
	{
		protected $alternative;

		public function __construct()
		{
			// Discourse compatibility
			if (file_exists('/usr/lib64/libjemalloc.so.2') && is_file('/usr/lib64/libjemalloc.so.2')) {
				$this->alternatives = [
					[
						'name'     => 'libjemalloc.so',
						'src'      => '/usr/lib64/libjemalloc.so.1',
						'dest'     => '/usr/lib64/libjemalloc.so.2',
						'priority' => 10
					]
				];

			}
		}
	}