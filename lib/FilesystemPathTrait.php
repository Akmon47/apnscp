<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	trait FilesystemPathTrait
	{
		/*
		 * @var string site account site index, '' for app admin/reseller, siteXX for account
		 */
		protected $site;

		/**
		 * Bind a site path context
		 *
		 * @param Auth_Info_User $auth
		 */
		protected function bindPathContext(\Auth_Info_User $auth): void
		{
			$this->site = $auth->site;
		}

		/**
		 * Create filesystem path
		 *
		 * @param string|null $subpath optional subpath to append
		 * @return string
		 */
		protected function domain_fs_path(string $subpath = null): string
		{
			\assert(null !== $this->site || !empty($_SESSION['site_id']));

			return FILESYSTEM_VIRTBASE . '/' . ($this->site ?? ('site' . $_SESSION['site_id']))
				. '/fst' . ($subpath ? ($subpath[0] === '/' ? '' : '/') . $subpath : '');
		}

		/**
		 * Clear domain FS path
		 *
		 * @param string|null $subpath
		 * @return string
		 */
		protected function freshen_domain_fs_path(string $subpath = null): string
		{
			$path = $this->domain_fs_path($subpath);
			clearstatcache(true, $path);
			return $path;
		}

		/**
		 * Create info path
		 *
		 * @param string|null $subpath optional subpath to append
		 * @return string
		 */
		protected function domain_info_path(string $subpath = null): string
		{
			\assert(null !== $this->site || !empty($_SESSION['site_id']));

			return FILESYSTEM_VIRTBASE . '/' . ($this->site ?? ('site' . $_SESSION['site_id']))
				. '/info' . ($subpath ? ($subpath[0] === '/' ? '' : '/') . $subpath : '');
		}

		/**
		 * Create shadow path
		 *
		 * @param string|null $subpath optional subpath to append
		 * @return string
		 */
		protected function domain_shadow_path(string $subpath = null): string
		{
			\assert(null !== $this->site || !empty($_SESSION['site_id']));

			return FILESYSTEM_VIRTBASE . '/' . ($this->site ?? ('site' . $_SESSION['site_id']))
				. '/shadow' . ($subpath ? ($subpath[0] === '/' ? '' : '/') . $subpath : '');
		}

		/**
		 * Construct a domain fs base path for a domain
		 *
		 * @param string|null $domain
		 * @return string path
		 */
		protected function make_domain_fs_path($domain = null): string
		{
			if (!$domain) {
				return '/';
			}
			$siteid = \Auth::get_site_id_from_domain($domain);
			if (!$siteid) {
				fatal("cannot make domain fs path, unknown domain `%s'", $domain);
			}

			return FILESYSTEM_VIRTBASE . '/site' . $siteid . '/fst';
		}

		protected function service_template_path($service = null): string
		{
			$path = FILESYSTEM_TEMPLATE;
			if ($service) {
				return $path . '/' . $service;
			}

			return $path;

		}
	}

