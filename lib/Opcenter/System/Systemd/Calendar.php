<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2020
 */

	namespace Opcenter\System\Systemd;

	use Opcenter\Versioning;

	class Calendar {
		/**
		 * Calendar spec conforms
		 *
		 * See systemd.time(7)
		 * @param string $format
		 * @return bool
		 */
		public static function valid(string $format): bool
		{
			if (!static::canVerify()) {
				return error("Calendar validation unsupported");
			}

			$ret = \Util_Process_Safe::exec('systemd-analyze calendar %s', [$format], [0], ['reporterror' => false]);
			return $ret['success'] ?: error($ret['stderr']);
		}

		/**
		 * Calendar verification supported
		 *
		 * @return bool
		 */
		public static function canVerify(): bool
		{
			return version_compare(Versioning::asMinor(os_version()), '8.0', '<');
		}
	}