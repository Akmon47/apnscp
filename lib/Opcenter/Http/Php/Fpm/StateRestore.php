<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
 */

namespace Opcenter\Http\Php\Fpm;

use Opcenter\Http\Php\Fpm;
use Opcenter\System\GenericSystemdService;

/**
 * Class StateRestore
 *
 * Track PHP-FPM pools after interaction
 *
 * @TODO support multiple pools per group
 *
 * @package Opcenter\Http\Php\Fpm
 */
class StateRestore {
	// @var bool shutdown or persist on end
	protected $terminateOnEnd = true;
	/**
	 * @var string
	 */
	protected $group;

	public function __construct(string $group)
	{
		if (posix_getuid()) {
			fatal('Requires privileged execution');
		}
		$this->group = $group;
		$this->terminateOnEnd = \count(GenericSystemdService::listAll($this->getPattern())) === 0;
	}

	/**
	 * Worker pattern
	 *
	 * @return string
	 */
	private function getPattern(): string
	{
		return Fpm::SERVICE_NAMESPACE . $this->group . '-*';
	}

	public function getGroup(): string
	{
		return $this->group;
	}

	/**
	 * Pools active or terminated
	 *
	 * @return bool
	 */
	public function getState(): bool
	{
		return !$this->terminateOnEnd;
	}

	public function __destruct()
	{
		if (!$this->terminateOnEnd) {
			return;
		}

		/**
		 * Knock out service but let socket reactivate
		 */
		$spec = Fpm::getServicePatternFromGroup($this->group);
		$service = SystemdSynthesizer::mock($spec);
		if (!$service::running()) {
			return;
		}
		debug('Terminating %s', $spec);
		$service::run('stop');
	}

}