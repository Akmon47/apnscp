<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2017
	 */

	namespace Opcenter\Mail\Vacation\Contracts;

	interface VacationOption
	{
		public function getValue(): ?string;

		public function setValue($value): bool;

		public function getFlag(): string;

		public function getInputName(): string;

		public function present(): bool;
	}