<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
	 */


	namespace Opcenter\Service\Validators\Billing;

	use Opcenter\Service\ServiceValidator;

	class Ctime extends ServiceValidator
	{
		const DESCRIPTION = 'Set account intake date';
		const VALUE_RANGE = '<int>';

		public function valid(&$value): bool
		{
			if ($value === null) {
				$value = time();
			}
			if (!\is_int($value)) {
				return error('Creation date must be unix timestamp');
			}
			if ($value > ($tmp = time())) {
				warn('Creation time newer than current timestamp? %d > %d', $value, $tmp);
			}

			return true;
		}
	}
