<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2021
 */

namespace Opcenter\Dns;

use Illuminate\Support\Collection;
use Opcenter\Account\Enumerate;

class Bulk
{
	/**
	 * @var Collection[apnscpFunctionInterceptor]
	 */
	protected Collection $sites;

	public function __construct(array $sites = null) {
		if (null === $sites) {
			$sites = Enumerate::sites();
		}

		$this->sites = collect($sites)->map(static function ($site) {
			if (!$site instanceof \Auth_Info_User) {
				if ($site instanceof \Auth_Info_Account) {
					$site = $site->site;
				}
				if (!($site = \Auth::nullableContext(null, $site))) {
					return null;
				}
			}
			$afi = \apnscpFunctionInterceptor::factory($site);
			return $afi->dns_configured() ? $afi : null;
		})->filter();
	}

	/**
	 * Add record
	 *
	 * @param Record        $r
	 * @param \Closure|null $where optional closure to evaluate application
	 * @return $this
	 */
	public function add(Record $r, \Closure $where = null): self {
		$i = 0;
		$this->sites->each(function (\apnscpFunctionInterceptor $afi) use ($r, &$i, $where) {
			$domains = array_keys($afi->web_list_domains());
			$ttl = $r['ttl'] ?? $afi->dns_get_default('ttl');
			debug("%d/%d addition - %d domains in batch", ++$i, $this->sites->count(), \count($domains));
			foreach($domains as $domain) {
				if ($afi->dns_parented($domain)) {
					continue;
				}

				if ($where) {
					$rCheck = clone $r;
					$rCheck['zone'] = $domain;
					if (!$where($afi, $rCheck)) {
						debug("Skipping %s - failed assertion", $domain);
						continue;
					}
				}

				$ret = false;
				try {
					$ret = $afi->dns_add_record(
						$domain,
						$r['name'],
						$r['rr'],
						$r['parameter'],
						$ttl
					);
				} catch (\Throwable $e) {
					warn("Failed on %(domain)s (provider: %(provider)s): %(msg)s", [
						'domain' => $domain,
						'provider' => $afi->dns_get_provider(),
						'msg' => $e->getMessage()
					]);
				} finally {
					if (!$ret) {
						warn(
							"Failed to set record on %s",
							$domain
						);
					}
				}
			}
		});

		return $this;
	}

	/**
	 * Remove record
	 *
	 * @param Record   $r
	 * @param \Closure|null $where optional closure to evaluate application
	 * @return $this
	 */
	public function remove(Record $r, \Closure $where = null): self {
		$i = 0;
		$this->sites->each(function (\apnscpFunctionInterceptor $afi) use ($r, &$i, $where) {
			$domains = array_keys($afi->web_list_domains());
			debug("%d/%d removal - %d domains in batch", ++$i, $this->sites->count(), \count($domains));
			foreach ($domains as $domain) {
				if ($afi->dns_parented($domain)) {
					continue;
				}

				if ($where) {
					$rCheck = clone $r;
					$rCheck['zone'] = $domain;
					if (!$where($afi, $rCheck)) {
						debug("Skipping %s - failed assertion", $domain);
						continue;
					}
				}

				$ret = false;
				try {
					$ret = $afi->dns_remove_record(
						$domain,
						$r['name'],
						$r['rr'],
						$r['parameter']
					);
				} catch (\Throwable $e) {
					warn("Failed on %(domain)s (provider: %(provider)s): %(msg)s", [
						'domain' => $domain,
						'provider' => $afi->dns_get_provider(),
						'msg' => $e->getMessage()
					]);
				} finally {
					if (!$ret) {
						warn(
							"Failed to delete record on %s",
							$domain
						);
					}
				}
			}
		});

		return $this;
	}
}
