<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
 */

namespace Util\Process\Contracts;


	/**
	 * Class JobDaemon
	 *
	 * @package Lararia
	 */
	interface StandaloneService
	{
		/**
		 * Launch Horizon manager
		 *
		 * @return bool
		 */
		public function start(): bool;

		/**
		 * Get job runner command name
		 *
		 * @return string
		 */
		public function getCommandName(): string;

		/**
		 * Verify Horizon running
		 *
		 * @return bool
		 */
		public function running(): bool;

		/**
		 * Get Horizon PID
		 *
		 * @return int|null
		 */
		public function getPid(): ?int;

		/**
		 * @param bool $force
		 * @return bool
		 */
		public function stop(bool $force = false): bool;
	}