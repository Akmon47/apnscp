<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 *
	 * @param string $fn
	 * @param array  $args
	 */

	use Lararia\Http\JsonResponse;

	if (($_SERVER['CONTENT_TYPE'] ?? '') === 'application/json') {
		if ((int)$_SERVER['CONTENT_LENGTH'] > 1024**3*10 /* 10 MB */) {
			fatal("Request size too large for application/json");
		}
		$_POST = (array)json_decode(file_get_contents('php://input'), true, 512, JSON_THROW_ON_ERROR);
	}

	$session_name = session_name();
	$session_id = null;
	if (isset($_POST[$session_name])) {
		$session_id  = $_POST[$session_name];
		unset($_POST[$session_name]);
	} else if (isset($_GET[$session_name])) {
		$session_id = $_GET[$session_name];
		unset($_GET[$session_name]);
	}

	if ($session_id && $session_id !== \session_id()) {
		if (null === ($auth = \Auth::autoload()->setID($session_id))) {
			// authentication expired
			http_response_code(403);
			exit();
		}
		Auth::set_handler($auth);
		\apnscpFunctionInterceptor::init(true);
	}
	if (!defined('AJAX')) {
		define('AJAX', 1);
		include(INCLUDE_PATH . '/lib/apnscpcore.php');
		set_time_limit(0);
	}
	if (!isset($_GET['fn'])) {
		return '';
	}

	$ajax_function = str_replace([':','-'], '_', $_GET['fn']);
	unset($_GET['fn']);
	if (AJAX && !headers_sent()) {
		// ensure if Accept: application/json is sent, it's returned as such
		header('Content-type: application/json', true);
	}

	try {
		if (isset($_POST['args'])) {
			$args = $_POST['args'];
		} else {
			$args = array_merge($_GET, $_POST);
		}
		if (!isset($_GET['engine']) && function_exists($ajax_function)) {
			$rfxn = new ReflectionFunction($ajax_function);
			if (!$rfxn->isUserDefined()) {
				// check to ensure user-defined, avoid fn=file_get_contents&args=/tmp/secret.key
				// allow some crappy low-level functions to be called, chiefly *-ajax.php scripts
				header('HTTP/1.1 403 Forbidden', true, 403);
				return;
			} else if (0 !== strpos($rfxn->getFileName(), webapp_path())) {
				// block access to functions that may reside in the global level
				// bad coding usually *raises hand*
				header('HTTP/1.1 403 Forbidden', true, 403);
				return;
			}

			print json_encode(call_user_func_array($ajax_function, $args));
			return;
		} else {
			$afi = \apnscpFunctionInterceptor::init();
			// these are virutal calls that flow through to Module_Skeleton
			if (method_exists($afi, $ajax_function)) {
				// disallow invoking MS methods directly, e.g. fn=set_level&args=[8]
				$response = error('dynamic dispatch error, attempt to call non-virtual method');
			} else {
				$args = $afi->polyfill($ajax_function, $args);
				$response = $afi->call($ajax_function, $args);
			}
		}
		$respObj = new JsonResponse($response);
	} catch (Exception $e) {
		$respObj = new JsonResponse($e);
	}

	if (!$respObj->success()) {
		$header = 'HTTP/1.1 ' . $respObj->getStatusCode() . ' Internal Server Error';
		header($header, true, $respObj->getStatusCode());
	}

	echo (string)$respObj;
