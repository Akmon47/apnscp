<?php
/** @var \apps\webapps\Page $Page */
/**
 * @var \Module\Support\Webapps\App\Unknown
 */
$app = $Page->getAppInstance();
$pane = $proxy->get($app->getHostname(), $app->getPath());
// not always the same as docroot
?>
<div class="row">
	<div class="col-12 text-right">
		<label>Change Mode:</label>
		<a class="ui-action ui-action-switch-app ui-action-label"
		   href="{!! \HTML_Kit::new_page_url_params(null, array('hostname' => null, 'path' => null)) !!}">Back to Site
			Listing</a>
	</div>
</div>

<?php
$formAction = \HTML_Kit::page_url_params();
$meta = $Page->getApplicationInfo($app->getDocumentMetaPath());
?>
@if (!empty($meta['failed']))
	<div class="alert alert-warning">
		<p>
			This application failed its last auto-update. Ensure that your application is installed
			correctly. To clear this message, attempt another update. Upon completion, your application
			will be eligible for automatic updates once again.
		</p>
		<form method="post" action="{!! $formAction !!}">
			<button type="submit" id="updateNote"
			        class="ajax-wait btn btn-secondary ui-action ui-action-label" name="update"
			        value="{{$app->getDocumentMetaPath()}}">
				<i class="ui-action ui-action-update"></i>
				Update
			</button>
			<button type="submit" class="btn btn-secondary ui-action ui-action-label" name="clear"
			        value="{{$app->getDocumentMetaPath()}}">
				<i class="fa fa-check"></i>
				Reset Failed Status
			</button>
		</form>

	</div>
@endif
<form method="post" action="{{ $formAction }}" class="" data-toggle="validator">
<div class="row">
	<!-- app meta container -->
	<div class="col-12 push-sm-6 col-sm-6 push-md-8 col-md-4 text-center">
		@include('partials.app-meta', [
			'appPath' => (!$Page->installPending() && $app->getModuleName()) ? $app->getAppRoot() : $app->getDocumentMetaPath()
		])
		@includeWhen(!$app->getModuleName(), 'partials.options-wrapper')
	</div>

		<div class="actions col-12 pull-sm-6 col-sm-6 pull-md-4 col-md-8">
			@if (!$Page->installPending())
				<h4 class="d-flex align-content-around align-items-center header-actions">
					Actions
					<button type="button" class="ml-auto btn-sm btn btn-outline-secondary" data-toggle="collapse"
				        data-target="#helpTopics">
						<i class="fa-question fa"></i>
						Help
					</button>
				</h4>

				<hr />
			@endif

			<div class="row">
				<div class="actions-list col-12 mb-1">
					<div id="helpTopics" class="collapse" data-toggle="collapse">
						@include ('partials/help', ['app' => $app])
					</div>

					@includeWhen(!$Page->installPending() || $app->isInstalling(), 'partials.actions', ['app' => $app])
					@if (!$app->applicationPresent() && (!$Page->installPending() || !$app->isInstalling()))
						<h4 class="d-flex">
							Install application @if ($Page->installPending()) &ndash; {{ $pane->getLocation() }} @endif
						</h4>
						<hr />
						@include('partials.install-app-grid')
					@elseif ($Page->installPending() && $app->applicationPresent())
						@include('partials.detection-required')
					@endif
				</div>
			</div>
		@includeIf('@webapp(' . $app->getModuleName() . ')::extras')
		@includeWhen($app->applicationPresent() || $app->isInstalling(), 'partials.options-wrapper')
	</div>
</div>
</form>

