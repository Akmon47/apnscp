@component('theme::partials.app.modal')
	@slot('title')
		Duplicate Site
	@endslot
	@slot('id')
		duplicateModal
	@endslot
	<div>
		<i class="ui-ajax-loading-large ui-ajax-indicator ui-ajax-loading"></i>
		<div class="body fade">
			<p>
				Cloning a website will make a 1:1 copy, updating the URL as necessary.
				The destination path must be clear before an import will succeed.
			</p>
			<label class="d-block form-control-static">Clone into</label>
			<select name="duplicate" class="custom-select custom-select-lg">
			</select>
			<div class="help-block with-errors text-danger"></div>
			@if ($app->hasReconfigurable('migrate'))
				<hr/>
				<p class="note text-info">
					Use the
					<b class="ui-action ui-action-rename ui-action-label">Rename Site</b> feature to change the site URL.
				</p>
			@endif
		</div>
	</div>
	@slot('buttons')
		<button type="submit" name="duplicate-hostname" class="btn btn-primary restore ajax-wait">
			<i class="fa fa-pulse d-none"></i>
			Duplicate Site
		</button>
		<p class="ajax-response"></p>
	@endslot
@endcomponent