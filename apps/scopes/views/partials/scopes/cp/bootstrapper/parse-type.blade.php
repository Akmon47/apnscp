<div id="roleOptions" class="mb-3">
	@include('partials.scopes.cp.bootstrapper.role-enumeration')
</div>
<hr />
<label class="custom-switch custom-control mb-0 pl-0 mr-3 mt-3">
	<input type="hidden" name="args" value="0"/>
	<input type="checkbox" id="confirmChanges"
	class="custom-control-input self-submit" />
	<span class="custom-control-indicator align-items-center"></span>
	Confirm Changes
</label>

<div class="align-self-end d-inline-flex mt-3" id="bootstrapperSubmission">
	<div class="btn-group">
		<button type="submit" name="apply-changes" value="" id="changeApply" disabled
		        class="btn btn-primary ui-action ui-action-label ui-action-run"
		>Change + Apply Role
		</button>
		<button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown"
		        aria-haspopup="true" aria-expanded="false">
			<span class="sr-only">Toggle Dropdown</span>
		</button>
		<div class="dropdown-menu dropdown-menu-right">
			<button class="btn btn-block dropdown-item" name="apply-changes" disabled value="">
				Change Only
			</button>
			<button class="btn btn-block dropdown-item" name="run-bootstrapper" value="1">
				Change + Full Integrity Check
			</button>
			<div class="dropdown-divider"></div>
			<button type="button" name="restart-panel"
			        class="btn dropdown-item btn-block warn ui-action-refresh ui-action ui-action-label">
				Restart {{ PANEL_BRAND }}</button>
		</div>
	</div>
</div>