<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\useredit;

	class Page extends \apps\useradd\Page
	{
		protected function loadBlade(string $template = 'index', string $path = null): \BladeLite
		{
			$path = webapp_path('useradd/views');
			return parent::loadBlade($template, $path);
		}

	}