@if (!$Page->hasSSO())
	<p class="alert-info note w-100">
		Single sign-on is not available. Mail access requires manual login. Username is {{ \UCard::get()->getUser() . '@' . \UCard::get()->getDomain() }}
	</p>
@endif
<div class="panel webmail-container webmail-sqmail">
	<a id="sqmail-logo" class="hidden-sm-down webmail-logo webmail-link"
	   href="{{ \HTML_Kit::page_url_params(array('app' => 'sqmail')) }}"></a>
	<h4>SquirrelMail</h4>
	<a class="ui-action ui-action-label ui-action-visit-site webmail-link btn btn-primary"
	   href="{{ \HTML_Kit::page_url_params(array('app' => 'sqmail')) }}">
		Access SquirrelMail
	</a>
	<p class="url my-1">
		<i class="fa fa-cloud"></i>
		{{ $Page->getWebmailAlias('sqmail', false) }}
	</p>
	@if (\UCard::get()->hasPrivilege('site'))
		<button class="btn btn-secondary ui-action ui-action-label edit-url" id="change-sqmail"><span
					class="ui-action ui-action-edit"></span>Change Shortcut
		</button>
	@endif
	<a class="lightbox btn btn-outline-secondary ui-action ui-action-label ui-action-view-picture" title="SquirrelMail"
	   href="/images/apps/webmail/sqmail-screencap.png">
		View Screenshot
	</a>
</div>

<div class="panel webmail-container webmail-horde">
	<a id="horde-logo" class="hidden-sm-down webmail-logo webmail-link"
	   href="{{ \HTML_Kit::page_url_params(array('app' => 'horde')) }}"></a>
	<h4>Horde</h4>
	<a class="ui-action ui-action-label ui-action-visit-site webmail-link btn btn-primary"
	   href="{{ \HTML_Kit::page_url_params(array('app' => 'horde')) }}">Access Horde</a>
	<p class="url my-1">
		<i class="fa fa-cloud"></i>
		{{ $Page->getWebmailAlias('horde', false) }}
	</p>
	@if (\UCard::get()->hasPrivilege('site'))
	<button class="btn btn-secondary ui-action ui-action-label edit-url" id="change-horde"><span
				class="ui-action ui-action-edit"></span>
		Change Shortcut
	</button>
	@endif
	<a class="lightbox btn btn-outline-secondary ui-action ui-action-label ui-action-view-picture" title="Horde"
	   href="/images/apps/webmail/horde-screencap.png">
		View Screenshot
	</a>
</div>

<div class="panel webmail-container webmail-roundcube">
	<a id="roundcube-logo" class="hidden-sm-down webmail-logo webmail-link"
	   href="{{ \HTML_Kit::page_url_params(array('app' => 'roundcube')) }}"></a>
	<h4>RoundCube</h4>
	<a class="ui-action ui-action-label ui-action-visit-site webmail-link btn btn-primary"
	   href="{{ \HTML_Kit::page_url_params(array('app' => 'roundcube')) }}">
		Access RoundCube
	</a>
	<p class="url my-1">
		<i class="fa fa-cloud"></i>
		{{ $Page->getWebmailAlias('roundcube', false) }}
	</p>
	@if (\UCard::get()->hasPrivilege('site'))
	<button class="btn btn-secondary ui-action ui-action-label edit-url" id="change-roundcube"><span
				class="ui-action ui-action-edit"></span>Change Shortcut
	</button>
	@endif
	<a class="lightbox btn btn-outline-secondary ui-action ui-action-label ui-action-view-picture" title="RoundCube"
	   href="/images/apps/webmail/roundcube-screencap.png">View Screenshot</a>
</div>