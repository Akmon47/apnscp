<div class="row" id="messages">
	<div class="px-0 col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-xl-4 offset-xl-4">
		<div class="messages alert @if ($errors) alert-danger @else alert-info @endif alert-dismissible fade show"
		     role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>

			<h3 class="@if ($Page->errors_exist()) error @endif">
				@if ($Page->errors_exist()) Error @else Information @endif
			</h3>

			@if ($msgs)
			<ul class="list-unstyled">
				@foreach ($msgs as $error)
				<li/>
				{!! $error['message'] !!}
				@endforeach
			</ul>
			@endif
		</div>
	</div>
</div>