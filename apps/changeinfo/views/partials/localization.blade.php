<div id="localization" class="tab-pane" role="tabpanel" aria-labelledby="">
	<label class="form-control-static">
		Timezone
		@php $myTZ = $Page->getMyTimezone(); @endphp
		<select name="timezone" class="custom-select form-control">
			@foreach ($Page->getTimezones() as $tz)
				<option value="{{ $tz }}" @if ($tz === $myTZ) selected="SELECTED" @endif
					>{{ str_replace('_', ' ', $tz) }}</option>
			@endforeach
		</select>
	</label>


	<label class="form-control-static">
		Language
		@php $myLang = $Page->getMyLanguage(); @endphp
		<select name="language" class="custom-select form-control">
			@foreach ($Page->getLanguages() as $code => $lang)
				<option value="{{ $code }}" @if ($code === $myLang) selected="SELECTED" @endif
					>{{ $lang }}</option>
			@endforeach
		</select>
	</label>
</div>