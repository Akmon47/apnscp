<div class="py-1 hide" tabindex="-1" role="dialog"  id="cloneDomain">
	<form action="{{ HTML_Kit::page_url_params() }}" method="post">
		<h3>Select source</h3>
		<select name="src-domain" class="form-control custom-select mb-3"></select>
		<label class="custom-checkbox custom-control align-items-center d-flex">
			<input type="checkbox" name="confirm" id="confirmRecordImport" class="custom-control-input" />
			<span class="custom-control-indicator"></span>
			<span>
				All records on {{ $Page->getDomain() }} will be removed. Records from source
				<span class="source"></span> will be used.
			</span>
		</label>
		<input type="hidden" name="target-domain" value="{{ $Page->getDomain() }}" />
	</form>
</div>
